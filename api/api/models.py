#from api.modules import Inventory
from flask.globals import session
from sqlalchemy import text,column
from flask import request
from sqlalchemy.sql.expression import update
from api.db import db

class AuditLog(db.Model):
    __tablename__ = "AuditLog"
    # Fields
    id = db.Column(db.Integer, primary_key=True)
    msg = db.Column(db.String)
    created = db.Column(db.DateTime)

    @staticmethod
    def add(msg):
        addr = request.headers.getlist("X-Forwarded-For")
        addr = addr[0] if addr else ""
        msg = f"{msg}:{addr} {request.user_agent.platform} {request.user_agent.browser}"
        db.session.add(AuditLog(msg=msg))
        db.session.commit()

class Inventory(db.Model):
    __tablename__ = "Inventory"
    #Fields
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String)
    quantity = db.Column(db.Integer)
        
    @staticmethod
    def add(id,quantity,name):
        db.session.add(Inventory(id=id,name=name,quantity=quantity))
        db.session.commit() 
    
    @staticmethod
    def delete(id):
        db.session.delete(Inventory.query.filter(Inventory.id==id).first())
        db.session.commit()

    @staticmethod
    def updateDb(id,quantity,name):
        item = Inventory.query.get(id)
        if  quantity != None:
            item.quantity = quantity
        if name != None:
            item.name = name
        db.session.commit()
