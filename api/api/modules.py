from api.db import exec_to_json
from api.mixins import register, BaseMixin
from api.models import Inventory
#from sqlalchemy import SQLAlchemyError
from sqlalchemy.exc import SQLAlchemyError
from sqlite3 import IntegrityError
from sqlalchemy.orm.exc import UnmappedInstanceError
@register
class Sales(BaseMixin):
    # Base
    slug = "sales"

    @classmethod
    def handle_get_list(cls, *, kwargs):
        return exec_to_json("select 1 as c1, 2 as c2")

@register
class inventory(BaseMixin):
    #Base
    slug = "inventory"

    @classmethod
    def handle_post_add(cls,* , kwargs):
        try:
            Inventory.add(kwargs["id"],kwargs["quantity"],kwargs["name"])
        except IntegrityError:
            return {"error":"Db Error , the ID which you are trying to add exists already , do you want to update instead ?"}
        except KeyError:
            return {"error":"Please ensure that all of the three fields are added while adding information"}
        except SQLAlchemyError:
            return {"error":"Please Ensure that all three fields are present and the ID you are adding hasn't been added"}
 
        return {"error":"none"}

    @classmethod
    def handle_delete_remove(cls,*,kwargs):
        try:
            Inventory.delete(kwargs["id"])
            return {"error":"none"}
        except KeyError:
            return {"error":"Please enter Id"}
        except UnmappedInstanceError:
            return {"error":"The Id you are delete doesn't exist"}

    @classmethod
    def handle_patch_update(cls,*,kwargs):
        if 'name' not in kwargs:
            kwargs["name"] = None
        if 'quantity' not in kwargs:
            kwargs["quantity"] = None
        try:
            Inventory.updateDb(kwargs["id"],kwargs["quantity"],kwargs["name"])
            return {"error":"none"}        

        except AttributeError:
            return {"error":"The ID you are trying to update doesn't exist"}


        