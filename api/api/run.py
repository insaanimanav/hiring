from flask import Flask
from flask import request as g_request
from api.db import db
import api.models
import api.urls

app = Flask(__name__)
# Database
# Any models without a bindkey end up in this db.
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///db.sqlite"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SQLALCHEMY_ECHO"] = False
db.init_app(app)
with app.test_request_context():
        db.create_all()
app.secret_key = "some secret"

# Blueprints

app.register_blueprint(api.urls.create_blueprint(), url_prefix="/api")

# Hooks


@app.after_request
def add_cors(response):
    origin = g_request.headers.get("Origin", "*")
    cors_string = "Origin, Accept , Content-Type, X-Requested-With, X-CSRF-Token"

    response.headers["Access-Control-Allow-Origin"] = origin
    response.headers["Access-Control-Allow-Headers"] = cors_string
    response.headers["Access-Control-Allow-Methods"] = "GET, POST, PATCH, DELETE"
    response.headers["Access-Control-Allow-Credentials"] = "true"
    return response


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
